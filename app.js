const express = require('express');
const apps = express();
const router = express.Router();
const bodyParser = require('body-parser');
const helmet = require('helmet');
const db = require('./src/config/db');
const account = require('./src/account/controllers/account');
const port = process.env.PORT || 7089;
var cors = require('cors');
const products = require('./src/Products/controllers/products');
db();

//cors implementation
apps.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    next();
});
// apps.use(cors);
apps.use(helmet());
apps.use(bodyParser.urlencoded());
apps.use(bodyParser.json());
apps.use('/api', router);
account(router);
products(router);


apps.get('/check', (req, res) => {
    res.send('working fine');
});


apps.listen(port);