const mongooseModel = require('mongoose');
const vouchersSchema = new mongooseModel.Schema({
    name: {
        type: String
    },
    code: {
        type: String
    },
    offer: {
        type: String
    },
    amount: {
        type: String
    },
    percentage: {
        type: String
    },
    type: {
        type: String
    },
    validAbove: {
        type: String
    },
    validFrom:{
      type: String
    },
    validTo : {
      type: String
    },
    appliedFor: [{
      type: String
    }]
},
{
  timestamps: true
});

const rolesDetails = mongooseModel.model('vouchers', vouchersSchema);

module.exports = rolesDetails;