const mongooseModel = require('mongoose');

const userGuestSchema = new mongooseModel.Schema({
    email: {
        type: String
    },
    phone: {
        type: String
    }
},
{
  timestamps: true
});    

const userGuestDetails = mongooseModel.model('guest', userGuestSchema);

module.exports = userGuestDetails;