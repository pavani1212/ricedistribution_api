const mongooseModel = require('mongoose');
const Schema = mongooseModel.Schema;
const usersSchema = new mongooseModel.Schema({
    firstName: {
        type: String,
        default: ''
    },
    lastName: {
        type: String,
        default: ''
    },
    phone: {
        type: String,
        default: ''
    },
    email: {
        type: String,
        default: ''
    },
    share_email: {
        type: String,
        default: ''
    },
    password: {
        type: String,
        default: ''
    },

    dob: {
        type: String,
        default: ''
    },
    city: {
        type: String,
        default: ''
    },
    country: {
        type: String,
        default: ''
    },
    street_name_number: {
        type: String,
        default: ''
    },
    apt: {
        type: String,
        default: ''
    },
    postCode: {
        type: String,
        default: ''
    },

    address: {
        type: String,
        default: ''
    },
    cart: [{
        type: Object
    }],
    wishlist: [{
        type: Object
    }],
    OrdersRefIds: [{
        type: Schema.Types.ObjectId,
        ref: 'bookings'
    }],
    status: {
        type: String,
        default: 'active'
    }
}, {
    timestamps: true
});

const usersDetails = mongooseModel.model('users', usersSchema);

module.exports = usersDetails;