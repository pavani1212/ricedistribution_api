const mongooseModel = require('mongoose');
const agentSchema = new mongooseModel.Schema({
    image: {
        type: String,
        default: ''
    },
    image_gallery: [{
        type: String
    }],
    name: {
        type: String
    },
    brand: {
        type: String
    },
    type: {
        type: String
    },
    description: {
        type: String
    },
    price: {
        type: String
    },
    quantity: {
        type: String
    },
    availability: {
        type: String
    }
}, {
    timestamps: true
});

const products = mongooseModel.model('products', agentSchema);

module.exports = products;