const helper = require('../../helpers/helper');
const vouchers = require('../../models/vouchers');
const mongoose = require('mongoose');
const UIDGenerator = require('uid-generator');
const uidgen = new UIDGenerator();

module.exports = () => {
    return {
        create: (req, res) => {
            let obj = req.body;
            vouchers.find({'code': req.body.code}, (err, data) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    if (data.length > 0) {
                        res.status(200).send((helper().responseSuccessMessage('1', 'Voucher code already exists', null, null)));
                    } else {
                        vouchers.create(obj, (err, voucherData) => {
                            if (err) {
                                res.status(500).send(err);
                            } else {
                                res.status(200).send((helper().responseSuccessMessage('1', 'Voucher created successfully', null, null)))
                            }

                        });
                    }
                }
            }); 
        },

        validate: (req, res) => {
            let obj = req.body;
            vouchers.find({'code': req.body.voucherCode}, (err, data) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    if (data.length > 0) {
                        res.status(200).send((helper().responseSuccessMessage('1', 'Voucher code already exists', null, null)));
                    } else {
                        vouchers.create(obj, (err, voucherData) => {
                            if (err) {
                                res.status(500).send(err);
                            } else {
                                res.status(200).send((helper().responseSuccessMessage('1', 'Staff member created successfully', null, null)))
                            }

                        });
                    }
                }
            }); 
        },

        getAllVouchers: (req, res) => {
            vouchers.find({}, (err, data) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.status(200).send((helper().responseSuccessMessage('1', 'Roles fetched successfully', data, null)))
                }
            });
        }, 

        getSingleVoucher: (req, res) => {
            vouchers.findById({'_id': mongoose.Types.ObjectId(req.body.id)}, (err, data) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.status(200).send((helper().responseSuccessMessage('1', 'Role fetched successfully', data, null)))
                }
            });
        },

        updateVoucher: (req, res) => {
            vouchers.findByIdAndUpdate({'_id': mongoose.Types.ObjectId(req.params.id)}, { $set: req.body }, (err, data) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.status(200).send((helper().responseSuccessMessage('1', 'Role fetched successfully', data, null)))
                }
            });
        }
    }
}                
