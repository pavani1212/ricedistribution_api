const voucherService = require('../services/voucher');
module.exports = (router) => {
  router.post('/voucher/create', (req, res) => {
    console.log(req.body);
    voucherService().create(req, res);
  });
  router.post('/voucher/reedem', (req, res) => {
    voucherService().reedem(req, res);
  });
  router.post('/voucher/validate', (req, res) => {
    voucherService().validate(req, res);
  });
  router.post('/voucher/delete', (req, res) => {
    voucherService().delete(req, res);
  });
  router.get('/voucher/getAll', (req, res) => {
    voucherService().getAllVouchers(req, res);
  });
  router.get('/voucher/getActive', (req, res) => {
    voucherService().getActive(req, res);
  });
  router.post('/voucher/getSingle', (req, res) => {
    voucherService().getSingleVoucher(req, res);
  });
  router.post('/voucher/update', (req, res) => {
    voucherService().updateVoucher(req, res);
  });
}

