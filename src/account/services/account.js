const helper = require('../../helpers/helper');
const users = require('../../models/users');
const guestUsers = require('../../models/guestusers');
const credentials = require('../../config/credentials');
const mongoose = require('mongoose');
const mailer = require('pug-mailer');
const CryptoJS = require("crypto-js");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('../../config/config');
const saltRounds = 10;

module.exports = () => {
    return {
        login: (req, res) => {
            users.find({ 'email': req.body.email, 'password': req.body.password }, (err, data) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    if (data.length > 0) {
                        res.status(200).send((helper().responseSuccessMessage('1', 'Login Successful', data, null)))

                    } else {
                        res.status(200).send((helper().responseSuccessMessage('0', 'Invalid Credentials', null, null)))
                    }
                }
            })
        },
        create: (req, res) => {
            let obj = req.body;
            users.find({ 'email': req.body.email }, (err, data) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    if (data.length > 0) {
                        res.status(200).send((helper().responseSuccessMessage('1', 'User already exists', null, null)));
                    } else {
                        users.create(obj, (err, data) => {
                            if (err) {
                                res.status(500).send(err);
                            } else {
                                res.status(200).send((helper().responseSuccessMessage('1', 'User created successfully', data, null)))
                            }
                        });
                    }
                }
            });
        },
        createGuest: (req, res) => {
            users.create(req.body, (err, data) => {
                if (err) {
                    res.status(500).send(err);
                } else {

                    res.status(200).send((helper().responseSuccessMessage('1', 'Guest created successfully', data, signuptoken)))
                }
            });
        },
        forgot: (req, res) => {
            mailer.init({
                service: 'Gmail',
                auth: {
                    user: credentials().mailerUserName,
                    pass: credentials().mailerPassword
                }
            })
            mailer.send({
                from: credentials().mailerUserName,
                to: req.body.email,
                subject: 'Innobot',
                template: 'forgot',
                data: {
                    id: '985643219'
                }
            }).then(response =>
                res.status(200).send((helper().responseSuccessMessage('1', 'Data saved successfully', null, null)))
            ).catch(err =>
                res.status(500).send(helper().responseErrorMessage('0', 'Mail not sent', err))
            )
        },
        changePassword: (req, res) => {
            const oldPassword = CryptoJS.AES.decrypt(req.body.oldPassword, '123456$#@$^@1ERF').toString(CryptoJS.enc.Utf8);
            const newPassword = CryptoJS.AES.decrypt(req.body.newPassword, '123456$#@$^@1ERF').toString(CryptoJS.enc.Utf8);
            users.findById({ '_id': mongoose.Types.ObjectId(req.params.id) }, (err, data) => {
                if (err) {
                    res.send(err);
                } else {
                    bcrypt.compare(oldPassword, data.password, function(err, result) {
                        if (result) {
                            bcrypt.hash(newPassword, saltRounds, function(err, hash) {
                                if (err) {
                                    res.send(err);
                                } else {
                                    users.findByIdAndUpdate({ '_id': mongoose.Types.ObjectId(req.params.id) }, { "$set": { "password": hash } }, (err, data) => {
                                        if (err) {
                                            res.send(err);
                                        } else {
                                            res.status(200).send((helper().responseSuccessMessage('1', 'Password changed successfully', null, null)))
                                        }
                                    })
                                }
                            });
                        } else {
                            res.status(200).send((helper().responseSuccessMessage('0', 'You have entered wrong password', null, null)))
                        }
                    });
                }
            });

        },
        getAllUsers: (req, res) => {
            users.find({}, (err, data) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.status(200).send((helper().responseSuccessMessage('1', 'Users fetched successfully', data, null)))
                }
            });
        },
        getSingleUser: (req, res) => {
            users.findById({ '_id': mongoose.Types.ObjectId(req.params.id) }, (err, data) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    console.log(data);
                    res.status(200).send((helper().responseSuccessMessage('1', 'User fetched successfully', data, null)))
                }
            });
        },
        updateUser: (req, res) => {
            console.log(req.body)
            users.findByIdAndUpdate({ '_id': mongoose.Types.ObjectId(req.params.id) }, { $set: req.body }, (err, data) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.status(200).send((helper().responseSuccessMessage('1', 'User updated successfully', data, null)))
                }
            });
        },
        getCartDetails: (req, res) => {
            users.find({ 'userId': mongoose.Types.ObjectId(req.params.id) }, (err, data) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.status(200).send((helper().responseSuccessMessage('1', 'User fetched successfully', data, null)))
                }
            });
        },
        addtocart: (req, res) => {
            users.find({ '_id': mongoose.Types.ObjectId(req.params.id) }, (err, data) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    console.log("inside cart")
                    console.log(data);
                    let cart = data[0].cart;
                    cart.push(req.body);
                    console.log("inside cart 170")

                    users.findByIdAndUpdate({ '_id': mongoose.Types.ObjectId(req.params.id) }, { $set: { "cart": req.body } }, (err, data) => {
                        if (err) {
                            res.status(500).send(err);
                        } else {
                            res.status(200).send((helper().responseSuccessMessage('1', 'cart updated successfully', data, null)))
                        }
                    });
                }
            });
        },
    }
}