const accountService = require('../services/account');
module.exports = (router) => {
    router.post('/login', (req, res) => {
        accountService().login(req, res);
    });
    router.post('/create', (req, res) => {
        accountService().create(req, res);
    });
    router.post('/createGuest', (req, res) => {
        accountService().createGuest(req, res);
    });
    router.post('/forgot', (req, res) => {
        accountService().forgot(req, res);
    });
    router.post('/changePassword/:id', (req, res) => {
        accountService().changePassword(req, res);
    });
    router.get('/getAllUsers', (req, res) => {
        accountService().getAllUsers(req, res);
    });
    router.get('/getSingleUser/:id', (req, res) => {
        accountService().getSingleUser(req, res);
    });
    router.post('/updateUser/:id', (req, res) => {
        accountService().updateUser(req, res);
    });

    router.get('/getCartDetails/:id', (req, res) => {
        accountService().getCartDetails(req, res);
    });

    router.post('/acc/add_to_cart/:id', (req, res) => {
        accountService().addtocart(req, res);
    });

    
}