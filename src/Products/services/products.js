const helper = require('../../helpers/helper');
const products = require('../../models/products');
const mongoose = require('mongoose');

module.exports = () => {
    return {

        create: (req, res) => {
            let obj = req.body;
            products.create(obj, (err, data) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.status(200).send({ "status": 200, data, "ok": true, message: "Product created successfully" })
                }
            });
        },



        getAllProducts: (req, res) => {
            products.find({}, (err, data) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.status(200).send({ "status": 200, data, "ok": true, message: "Products fetched successfully" })
                }
            });
        },
        getProductsByType: (req, res) => {
            let type = req.body.type
            products.find({ 'type': type }, (err, data) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.status(200).send((helper().responseSuccessMessage('1', 'Products fetched successfully', data, null)))
                }
            });
        },


        fetchone: (req, res) => {
            console.log('inside service');
            products.findById({ '_id': mongoose.Types.ObjectId(req.params.id) }, (err, data) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    console.log(data);
                    res.status(200).send({ "status": 200, data, "ok": true, message: "Products fetched successfully" })                }
            });
        },

        updateProduct: (req, res) => {
            products.findByIdAndUpdate({ '_id': mongoose.Types.ObjectId(req.params.id) }, { $set: req.body }, (err, data) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.status(200).send((helper().responseSuccessMessage('1', 'User updated successfully', data, null)))
                }
            });
        },

    }
}