const productService = require('../services/products');
module.exports = (router) => {
    router.get('/product/getAll', (req, res) => {
        productService().getAllProducts(req, res);
    });
    router.post('/product/create', (req, res) => {
        productService().create(req, res);
    });
    router.get('/product/fetchone/:id', (req, res) => {
        console.log('inside controller');

        productService().fetchone(req, res);
    });
    router.post('/product/getbyType/:id', (req, res) => {
        productService().getProductsByType(req, res);
    });
    router.post('/product/update/:id', (req, res) => {
        productService().updateProduct(req, res);
    });
    router.post('/product/delete/:id', (req, res) => {
        productService().deleteProduct(req, res);
    });

}