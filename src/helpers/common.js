const { _ } = require("lodash");
const { find, eqProps } = require("ramda");
module.exports = () => {
  return {
    combinePriceArr(obj, connectingTypeObj) {
      const connectingPriceArr = [];
      const multiPriceObj = {};
      let tripPriceObj;
      _.forEach(obj.totalPriceList, priceObj => {
        const adultPrice = priceObj.fd.ADULT
          ? Math.ceil(priceObj.fd.ADULT.fC.TF)
          : 0;
        const childPrice = priceObj.fd.CHILD
          ? Math.ceil(priceObj.fd.CHILD.fC.TF)
          : 0;
        const infantPrice = priceObj.fd.INFANT
          ? Math.ceil(priceObj.fd.INFANT.fC.TF)
          : 0;
        const tripJackPrice = adultPrice + childPrice + infantPrice;
        tripPriceObj = {};
        tripPriceObj.type = "tripJack";
        tripPriceObj.price = tripJackPrice;
        tripPriceObj.class = priceObj.fd.ADULT.cc;
        tripPriceObj.class = priceObj.fareIdentifier;
        tripPriceObj.reviewId = priceObj.id;
      });
      multiPriceObj.price = connectingTypeObj.TotalAmount;
      multiPriceObj.type = "multilink";
      multiPriceObj.availableSeats = connectingTypeObj.AvailSeats;
      multiPriceObj.reviewId = connectingTypeObj.TrackNo;
      connectingPriceArr.push(
        { tripJack: tripPriceObj },
        { multiLink: multiPriceObj }
      );
      return connectingPriceArr;
    }
  };
};
