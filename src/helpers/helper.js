module.exports = () => {
    return {
        responseSuccessMessage: (status, message, data, token) => {
            const obj = {}
            obj['success'] = status;
            obj['token'] = token;
            obj['message'] = message;
            obj['data'] = data;
            obj['status'] = status;
            return obj;
        },
        responseErrorMessage: (status, error) => {
            const obj = {}
            obj['success'] = status;
            // obj['message'] = message;
            obj['error'] = error;
            return obj;
        }
    }
}